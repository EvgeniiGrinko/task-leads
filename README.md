
Project address: http://localhost:4002

# To start project for the first time run following commands in terminal in project folder:
```shell
make compose-build
make compose-install-composer
make compose-up
make compose-copy-env
make compose-key
make compose-npm-i
make compose-npm-build
make compose-db-init
```

and then proceed to http://localhost:4002
Log in with email "admin@examle.com" and password "password"

# Available Commands
```shell
make compose-build      # Build the project

make compose-up         # Start the project
make compose-down       # Stop the project

make compose-db-init    # Initialize the database
make compose-db-migrate # Run migrations

make compose-sh-nginx # Enter the nginx container
make compose-sh-php   # ВEnter the php container

```
