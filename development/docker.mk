compose-build:
	docker compose build

compose-up:
	docker compose up -d

compose-down:
	docker compose down

compose-copy-env:
	docker compose run --rm php cp .env.example .env

compose-key:
	docker compose run --rm php php artisan key:generate

compose-npm-i:
	docker compose run --rm php npm i

compose-install-composer:
	docker compose run --rm php composer install
	docker compose run --rm php composer dump-autoload

compose-npm-build:
	docker compose run --rm php npm run build

compose-db-init:
	docker compose run --rm php php artisan migrate:refresh --force
	docker compose run --rm php php artisan db:seed --force

compose-db-migrate:
	docker compose run --rm php php artisan migrate

compose-sh-nginx:
	docker compose run --rm nginx sh

compose-sh-php:
	docker compose run --rm php sh
